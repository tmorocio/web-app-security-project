import ZeroBankPage from '../Page/zeroBankPage';
import ZeroBankFlow from '../Flow/zeroBankFlow';

describe('Web App Security Site', function() {
    it('Transfer money and make funds', function() {

         browser.url('http://zero.webappsecurity.com');
         browser.maximizeWindow();
         console.log('Zero Bank Home Page');

         new ZeroBankPage()
            .transferMoney()
            

        //browser.debug();
        
    })

     it('Make payments to save payees money and make funds', function() {

         browser.url('http://zero.webappsecurity.com');
         browser.maximizeWindow();
         console.log('Zero Bank Home Page');

         new ZeroBankPage()
            .payBills();
        
        // browser.debug();
       
    })

     it('Add New Payee', function() {

        browser.url('http://zero.webappsecurity.com');
        browser.maximizeWindow();
        console.log('Zero Bank Home Page');

        new ZeroBankPage()
            .newPayee();

        //browser.debug();

     })

     it('Add Purchase foreign currency cash Payee', function() {

        browser.url('http://zero.webappsecurity.com');
        browser.maximizeWindow();
        console.log('Zero Bank Home Page');

        new ZeroBankPage()
            .foreignCurrency();

        //browser.debug();

     })

}); 