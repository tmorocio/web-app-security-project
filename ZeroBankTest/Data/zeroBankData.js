export const transferData = {

    credentials: {
        username: 'username',
        password: 'password'
    },

    moneyTransfer: {
        amount: 100,
        description: 'test'
    },

    payBillData: {
        pAmount: 50,
        pDescription: 'test pay'

    },

    addPayee: {
        addName: 'Juan Dela Cruz',
        addAddress: '123 Tondo, Manila',
        addAccount: 'Test Account',
        addDetails: 'This is for test'

    }
}