import {transferData} from '../Data/zeroBankData';

class ZeroBankPage {

     get activeHomeTab()          { return $('//li[@class="active"][@id="homeMenu"]'); }
     get signInButton()           { return $('//button[@id="signin_button"]'); }
     get username()               { return $('//input[@id="user_login"]'); }
     get password()               { return $('//input[@id="user_password"]'); }
     get signInBttn()             { return $('//input[@name="submit"]'); }
     get accSummary()             { return $('//li[@id="account_summary_tab"]'); }
     get transferFunds()          { return $('//li[@id="transfer_funds_tab"]'); }
     get transferFundsTxt()       { return $('//*[@id="transfer_funds_content"]/form/div/div/h2'); }
     get amount()                 { return $('//input[@id="tf_amount"]'); }
     get description()            { return $('//input[@id="tf_description"]'); }
     get continueButton()         { return $('//button[@type="submit"]'); }
     get transferVerifyTxt()      { return $('//*[@id="transfer_funds_content"]/form/div/div/h2'); }
     get submitPrimary()          { return $('//button[@class="btn btn-primary"]'); }
     get transferConfirmText()    { return $('//*[@id="transfer_funds_content"]/div/div/h2'); }


     get accSummaryLink()        { return $('//*[@id="account_summary_link"]'); }
     get payBillsLink()          { return $('//span[@id="pay_bills_link"]'); }
     get payAmout()              { return $('//*[@id="sp_amount"]'); }
     get dateSelect()            { return $('//*[@id="sp_date"]'); }
     get currentDate()           { return $('//*[@id="ui-datepicker-div"]/table/tbody/tr[4]/td[3]/a'); }
     get payDescription()        { return $('//*[@id="sp_description"]'); }
     get payButton()             { return $('//*[@id="pay_saved_payees"]'); }
     get alertPay()              { return $('//*[@id="alert_content"]'); }
     get moreServiceButton()     { return $('//*[@id="online-banking"]'); }
     get paybillsTab()           { return $('//*[@id="pay_bills_tab"]'); }


     get addNewPayee()           { return $('//a[contains(text(),"Add New Payee" )]'); }
     get payeeName()             { return $('//*[@id="np_new_payee_name"]'); }
     get payeeAddress()          { return $('//*[@id="np_new_payee_address"]'); }
     get account()               { return $('//*[@id="np_new_payee_account"]'); }
     get payeeDetails()          { return $('//*[@id="np_new_payee_details"]'); }
     get addPayeeBtn()           { return $('//*[@id="add_new_payee"]'); }
     get alertAdd()              { return $('//*[@id="alert_content"]'); }


     get foreignTab()            { return $('//*[@id="tabs"]/ul/li[3]'); }
     get currencyDropdown()      { return $('//*[@id="pc_currency"]'); }
     get currency()              { return $('//select[@id="pc_currency"]//option[@value="JPY"]'); }
     get currencyAmt()           { return $('//input[@id="pc_amount"]'); }
     get usd()                   { return $('//*[@id="pc_inDollars_true"]'); }
     get calculateBtn()          { return $('//input[@id="pc_calculate_costs"]'); }
     get conversionAmt()         { return $('//label[@id="pc_conversion_amount"]'); }  
     get purchaseBtn()           { return $('//input[@id="purchase_cash"]'); } 



     transferMoney() {
         this.activeHomeTab.waitForDisplayed(1000);
         this.signInButton.click();
         this.username.setValue(transferData.credentials.username);
         this.password.setValue(transferData.credentials.password);
         this.signInBttn.click();
         this.accSummary.waitForDisplayed(1000);
         console.log('Account Summary displayed');
         this.transferFunds.click();
         this.transferFundsTxt.waitForDisplayed(1000);
         console.log('Transfer Funds Page');
         this.amount.setValue(transferData.moneyTransfer.amount);
         this.description.setValue(transferData.moneyTransfer.description);
         this.continueButton.click();
         this.transferVerifyTxt.waitForDisplayed(1000);
         console.log('Transfer Money & Make Payments - Verify Page Displayed');
         this.submitPrimary.click();
         this.transferConfirmText.waitForDisplayed(1000);
         console.log('Transfer Money & Make Payments - Confirm Page Displayed');  

     }

        
     payBills() {
         this.moreServiceButton.waitForDisplayed(1000);
         this.moreServiceButton.click();
         this.accSummaryLink.click();
         console.log('Account Summary displayed');
         //this.payBillsLink.click();
         this.paybillsTab.click();
         this.payAmout.setValue(transferData.payBillData.pAmount);
         this.dateSelect.click();
         this.currentDate.click();
         this.payDescription.setValue(transferData.payBillData.pDescription);
         this.payButton.click();
         this.alertPay.waitForDisplayed(1000);
         console.log('The payment was successfully submitted.');

     }

     newPayee() {
        this.moreServiceButton.waitForDisplayed(1000);
        this.moreServiceButton.click();
        this.accSummaryLink.click();
        console.log('Account Summary displayed');
        this.paybillsTab.click();
        this.addNewPayee.click();
        this.payeeName.setValue(transferData.addPayee.addName);
        this.payeeAddress.setValue(transferData.addPayee.addAddress);
        this.account.setValue(transferData.addPayee.addAccount);
        this.payeeDetails.setValue(transferData.addPayee.addDetails);
        this.addPayeeBtn.click();
        this.alertAdd.waitForDisplayed(1000);

     }

     foreignCurrency() {
        this.moreServiceButton.waitForDisplayed(1000);
        this.moreServiceButton.click();
        this.accSummaryLink.click();
        console.log('Account Summary displayed');
        this.paybillsTab.click();
        this.foreignTab.click();
        this.currencyDropdown.click();
        this.currency.click();
        this.currencyAmt.setValue(50);
        this.usd.click();
        this.calculateBtn.click();
        this.conversionAmt.waitForDisplayed(1000);
        this.purchaseBtn.click();
        this.alertAdd.waitForDisplayed(1000);

     }




}

export default ZeroBankPage;
//export default (ZeroBankPage = new ZeroBankPage());